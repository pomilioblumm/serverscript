#!/bin/bash

echo "Enter the username:"
read USER
echo "Enter the website"
read SITE

useradd -G www-data -ms /bin/false $USER
passwd $USER
mkdir -p /home/$USER/sites/$SITE
chown -R $USER:www-data /home/$USER/sites
chown root:root /home/$USER/
chown root:root /home/
chmod g+s /var/www/$SITE/htdocs/
chmod 775 /var/www/$SITE/htdocs
mount --bind /var/www/$SITE /home/$USER/sites/$SITE
echo "/var/www/$SITE /home/$USER/sites/$SITE none defaults,bind 0 0" >> /etc/fstab
